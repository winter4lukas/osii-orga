# Open Source Imaging Initiative (OSI²) - Orga

## Overview

This is the organizational space for the Open Source Imaging Initiative (www.opensourceimaging.org). If you want to contribute, have a look at the following information. 

## Open-source Projects on Opensourceimaging.org

A list of current open-source projects can be found below. 

### Update project information
If you would like to update a project, please create an issue with relevant information here: 

### Add projects
If you would like to add your project to be highlighted on the website, please use a template and add it to this folder here over a commit.

## List of open-source software
Name | Description | License | License Type | 
-----|-----|-----|-----|

## List of open-source hardware
Name | Description | License | License Type | OS Score [%] | Native files | Export files | BoM | Instructions | Complete | Untrapped | Reproduced | Reference
-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|
[COSI Measure](https://www.opensourceimaging.org/project/cosi-measure/) | 3-axis measurement system | CERN-OHL-1.2 | strong copyleft | 100 | Yes | Yes | Yes | Yes | Yes | Yes | Yes | [v1.0](https://github.com/opensourceimaging/cosi-measure/)

